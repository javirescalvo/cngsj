<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;

class Hermano extends Model
{
    use Notifiable;

    /**
     *
     * Tabla
     *
     * @var string
     *
     */
    protected $table = "hermanos";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','apellido1','apellido2','telefono1','telefono2','email','direccion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     *
     * Poblacion donde vive el hermano
     *
     * @return BelongsTo
     *
     */
    public function poblacion()
    {
        return $this->belongsTo('App\Poblacion');
    }


    /**
     *
     * Asignaciones del hermano, ej. territorios, turnos de exhibidor, reuniones
     *
     * @return HasMany
     *
     */
    public function asignacion()
    {
        return $this->hasMany('App\Asignacion');
    }


    /**
     *
     * Privilegios del hermano, ej. servicio, responsabilidades
     *
     * @return HasMany
     *
     */
    public function privilegio()
    {
        return $this->hasMany('App\Privilegio');
    }

}
