<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Territorio extends Model
{
    /**
     *
     * Tabla
     *
     * @var string
     *
     */
    protected $table = "Territorios";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','codigo','referencia','comentarios'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     *
     * Reuniones celebradas en la poblacion
     *
     * @return BelongsTo
     *
     */
    public function poblacion()
    {
        return $this->belongsTo('App\Poblacion');
    }
}