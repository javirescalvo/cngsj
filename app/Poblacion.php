<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Poblacion extends Model
{
    /**
     *
     * Tabla
     *
     * @var string
     *
     */
    protected $table = "poblaciones";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','codigo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     *
     * Reuniones celebradas en la poblacion
     *
     * @return HasMany
     *
     */
    public function reunion()
    {
        return $this->hasMany('App\Reunion');
    }


    /**
     *
     * Hermanos que viven en la poblacion
     *
     * @return HasMany
     *
     */
    public function hermano()
    {
        return $this->hasMany('App\Hermano');
    }


    /**
     *
     * Grupo de la poblacion
     *
     * @return HasOne
     *
     */
    public function grupo()
    {
        return $this->hasOne('App\Grupo');
    }

}