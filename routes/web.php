<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index');
Route::get('/hermanos','HermanosController@index')->middleware('auth');
Route::get('/hermanos/import','HermanosController@import')->middleware('auth');
Route::get('/hermanos/ver/{id}','HermanosController@show')->middleware('auth')->where("id","[0-9]+");
Route::get('/hermanos/editar/{id}','HermanosController@edit')->middleware('auth')->where("id","[0-9]+");
Route::post('/hermanos/editar/{id}','HermanosController@update')->middleware('auth')->where("id","[0-9]+");